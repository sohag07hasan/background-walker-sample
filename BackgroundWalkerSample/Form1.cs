﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BackgroundWalkerSample
{
    public partial class Form1 : Form
    {
        private BackgroundWorker myWorker = new BackgroundWorker();
        public Form1()
        {
            InitializeComponent();
            myWorker.DoWork += new DoWorkEventHandler(myWorker_DoWork);
            myWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(myWorker_RunWorkerCompleted);
            myWorker.ProgressChanged += new ProgressChangedEventHandler(myWorker_ProgressChanged);
            myWorker.WorkerReportsProgress = true;
            myWorker.WorkerSupportsCancellation = true;
        }        

        private void toolStripStatusLabel1_Click_1(object sender, EventArgs e)
        {
            
        }

        protected void myWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker sendingWorker = (BackgroundWorker)sender;//Capture the BackgroundWorker that fired the event
            object[] arrObjects =
            (object[])e.Argument;//Collect the array of objects the we received from the main thread

            int maxValue = (int)arrObjects[0];//Get the numeric value 
            //from inside the objects array, don't forget to cast
            StringBuilder sb = new StringBuilder();//Declare a new string builder to store the result.

            for (int i = 1; i <= maxValue; i++)//Start a for loop
            {
                if (!sendingWorker.CancellationPending)//At each iteration of the loop, 
                //check if there is a cancellation request pending 
                {
                    sb.Append(string.Format("Counting number: {0}{1}",
                    PerformHeavyOperation(i), Environment.NewLine));//Append the result to the string builder
                    sendingWorker.ReportProgress(i);//Report our progress to the main thread
                }
                else
                {
                    e.Cancel = true;//If a cancellation request is pending, assign this flag a value of true
                    break;// If a cancellation request is pending, break to exit the loop
                }
            }

            e.Result = sb.ToString();// Send our result to the main thread!
        }

        protected void myWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if (!e.Cancelled && e.Error == null)//Check if the worker has been canceled or if an error occurred
            {
                string result = (string)e.Result;//Get the result from the background thread
                textBox1.Text = result;//Display the result to the user
                toolStripStatusLabel1.Text = "Done";
            }
            else if (e.Cancelled)
            {
                toolStripStatusLabel1.Text = "User Canceled";
            }
            else
            {
                toolStripStatusLabel1.Text = "An error has occurred";
            }
            button1.Enabled = true;//Re enable the start button
        }

        protected void myWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripStatusLabel1.Text = string.Format("Counting number: {0}...", e.ProgressPercentage);
        }

        private int PerformHeavyOperation(int i)
        {
            System.Threading.Thread.Sleep(250);
            return i * 1000;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int numericValue = (int)numericUpDown1.Value;//Capture the user input
            object[] arrObjects = new object[] { numericValue };//Declare the array of objects
            if (!myWorker.IsBusy)//Check if the worker is already in progress
            {
                button1.Enabled = false;//Disable the Start button
                myWorker.RunWorkerAsync(arrObjects);//Call the background worker
            }
        }
    }
}
